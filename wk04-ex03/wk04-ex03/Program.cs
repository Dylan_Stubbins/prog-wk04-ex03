﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wk04_ex03
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter in a number");
            var input = Console.ReadLine();
            var a = 0;
            bool value = int.TryParse(input, out a);

            if (value)
            {
                Console.WriteLine($"Well done, you entered correctly.");
            }
            else
            {
                Console.WriteLine($"Wrong. Read it again");
            }
        }
    }
}
